## random matrix: rand(rows, cols)
A = rand (3,2)

## transposed matrix: A'
B = A'

## To solve the set of linear equations Ax = b, use the left division operator,
## ‘\’:
b = rand(3,1)
x = A \ b

A = [ 2, 0; 0, 2 ];
b = [ 2; 1 ];
x = A \ b

## Integrating Differential Equations, like
##
## dx
## -- = f (x, t)
## dt
##
## with the initial condition
##
## x(t = t0) = x0
##

si
function xdot = f (x, t)

  r = 0.25;
  k = 1.4;
  a = 1.5;
  b = 0.16;
  c = 0.9;
  d = 0.8;

  xdot(1) = r*x(1)*(1 - x(1)/k) - a*x(1)*x(2)/(1 + b*x(1));
  xdot(2) = c*a*x(1)*x(2)/(1 + b*x(1)) - d*x(2);

endfunction

x0 = [1; 2];
t = linspace (0, 50, 200)';
x = lsode ("f", x0, t);

plot (t, x)

##########

x = 1:0.1:5;
function y = poly(x)
         for i = 1 50
               y(i)=[x(i), x(i)^3 + x(i)^2+x+2];
         endfor
endfunction


y= x*x*x+x*x+x+2;
plot (x,poly(x),'g');
title ('plot() of green line at 45 degrees');


global a = 1
global b = 0
global c = 0

function z = polynomial (i,j)
  global a
  global b
  global c
  x = i + (j * 1j);
  z = a*x*x + b*x + c;
end


function ret = funName(x)
  ret=x
end
