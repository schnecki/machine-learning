function newData = randomlyRemoveDatapoints(data, percentage)
  ## The data points which might randomly loose data points and the percentage
  ## to be kept.

  ## check for user input of percentage
  if percentage > 1 | percentage < 0
    error("percentage must be in [0.0, 1.0].")
  endif

  ## randomly remove data points
  j = 1;
  x = [];
  y = [];
  for i = 1 : rows(data)
    if rand(1, 1) < percentage
      x(j) = data(i, 1);
      y(j) = data(i, 2);
      j++;
    endif
  endfor

  ## set new data
  newData = [x; y]';
endfunction
