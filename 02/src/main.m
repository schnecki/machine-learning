
function [data, Rdata, F, EV, ET, minDS, minL, lambda, fig] = main

  ################################################################################
  # Configuration
  ################################################################################
  ## The range of the x-axis of the plot
  M=3;                          # number of data sets
  N=10;                         # number of values per data set
  x0=-10;                       # left boundary on x-axis
  x1=10;                        # right boundary on x-axis

  sigma=50;                     # sigma for generating test data


  lambda = [0 : 100 : 10*sigma^2 ];
  ## lambda = [0 : 0.25 : sigma ];

  ## The coefficients of the polynomial, given as:
  ## (c(1) * x^(n-1) + c(2) * x^(n-2) + ... + c(end))
  ## were n is the length of the vector c. The minimal length is 2.
  c = [0.3, .0, -2, 3434];

  substractMean=1;                # whether to subtract mean form y-values, 1..yes

  phi = @(x)[1                    # starting with w0
             x
             x^2
             ## x^3
             ## x^4
             ## x^5
             ## x^6
             ## x^7
             ## x^8
             ## x^9
             ## x^10
             ## x^11
             ## x^12
             ## x^13
             ## x^14
             ## x^15
            ];

  verbose=1;                    # verbose output, plot everything


  ################################################################################
  # End of Configuration
  ################################################################################


  ## Calculate step and create data points
  N0 = M*N-1;
  step=(x1 - x0) / N0;
  x = [x0:step:x1];

  ## Create polynomial
  p = polyval(c, x);
  ## p = 500*sin(x);


  warning("off", "all");

  lambda_len = columns(lambda);

  K=length(c) - 1;

  if K<2
    error("Minimum order of polynomial function: 2");
    exit(1);
  endif

  if M <2
    error("There need to be at least 2 data sets.");
    exit(1);
  endif

  if min(lambda) < 0
    error("Lambda needs to be >= 0.");
    exit(1);
  endif


  ## combine to a matrix
  data = [x; p]';

  ## generate random data point
  Rdata = addepsilon(sigma, data);

  if substractMean
    ## Calculate average y-values
    avg=mean(Rdata(:,2));

    ## Reduce y-values by average
    Rdata = [ Rdata(:,1), Rdata(:,2) .- avg ];
  endif

  ## Generate data sets
  for i = 1 : M*N
    m = floor((i-1)/M) + 1;
    n = 2*mod(i-1, M);
    R(m, n+1) = Rdata(i, 1);
    R(m, n+2) = Rdata(i, 2);
  endfor


  ## add average to data points for plotting
  if substractMean
    Rdata = [ Rdata(:,1), Rdata(:,2) .+ avg ];
  endif

  ## set flags
  errorVals=[];
  errorTest=[];
  fig = 1;


  ## do each data set
  for i = 1 : M

    ## generate validation set
    tSet = [];
    vSet = [];
    vSet = [R(:,2*i-1), R(:,2*i)];


    ## generate test set
    for j = 1 : M
      if i==j
        continue;
      endif;
      tSet = vertcat(tSet, [R(:,2*j-1), R(:, 2*j)]);
    endfor

    ## setup new plot
    if verbose
      figure(fig++);
      clf;
      hold on;

      plot (data(:,1), data(:,2), "r");
      plot (tSet(:,1), tSet(:,2) .+ avg, "r+");
      plot (vSet(:,1), vSet(:,2) .+ avg, "gv");
      xlabel ("x");
      ylabel ("y");

      title (horzcat("Data-set No. ", num2str(i),
                     ". Test/Validation points generated with a standard derivation of ",
                     num2str(sigma), ".\nMaximum degree of polynomial r(x, w, l) is ",
                     num2str(length(phi(0))-1), "."));
    endif

    ## Sort the set for nice plotting (otherwise the graph is hoping back and
    ## forth)
    tSet = sortrows(tSet);

    ## calculate the design matrix
    D=design_matrix(phi, tSet);

    ## train with several lambdas
    for l = 1 : lambda_len

      ## pseudo inverse
      Dt=pseudo(lambda(l), D);

      ## calculate coefficients
      w=Dt*tSet(:, 2);

      ## add mean again to w0
      if substractMean
        w(1) += avg;
      end

      ## flip vector
      wf=flipud(w)';

      ## calculate error wrt the validation set vSet
      err = 0.0;
      for m = 1 : rows(vSet)
        err += (vSet(m,2) - phi(vSet(m,1))' * w)^2;
      endfor
      err *= 0.5;
      ## err += lambda(l) * sum(w.^2);
      errorVals(i, l) = err;


      ## calculate error wrt the test set tSet
      err = 0.0;
      for m = 1 : rows(tSet)
        err += (tSet(m,2) - phi(tSet(m,1))' * w)^2;
      endfor
      err *= 0.5;
      ## err += lambda(l) * sum(w.^2);
      errorTest(i, l) = err;


      ## generate polynomial
      f=polyval(wf, data(:,1));

      ## save data of F for later reference
      F(:,(i-1) * lambda_len + l) = f';

      ## plot if verbose mode is on
      if verbose
        plot (data(:,1), f(:,1));
      endif
    endfor

    name = num2str(c(end));
    for degree = 1 : length(c) - 1
      name = horzcat(strcat(num2str(c(length(c) - degree)), "*x^", num2str(degree), "+"), name);
    endfor

    legend(strcat("f(x)= ", name), ## num2str(length(c)-1)),
           strcat("test points"),
           "validation points",
           horzcat("r(x, w, l), lambda l=", num2str(lambda(1)), ", ",
                   num2str(lambda(2)), ", ..., ", num2str(lambda(end))));

    print(strcat("dataset", num2str(i), ".png"), "-color");
  endfor


  ## set error matrices
  for i=1 : rows(errorVals)
    ## error on validation set
    EV(:, 2*i-1) = lambda';
    EV(:, 2*i) = errorVals(i,:)';

    ## error on test set
    ET(:, 2*i-1) = lambda';
    ET(:, 2*i) = errorTest(i,:)';
  endfor

  ## normalize error by maximum of all errors
  evm = max(max(EV));
  for m = 1 : columns(EV)/2
      EV(:,2*m) = EV(:,2*m) ./ evm;
  endfor

  ## normalize error by maximum of all errors
  evm = max(max(ET));
  for m = 1 : columns(ET)/2
    ET(:, 2*m) = ET(:, 2*m) ./ evm;
  endfor

  ## find minimum error
  minDS = 1;
  minL = 1;
  minVal = EV(1,2);

  for i=1 : rows(EV)
    for j=1 : columns(EV)/2
      if EV(i,2*j) < minVal
        minDS = j;
        minL = i;
        minVal = EV(i,2*j);
      endif
    endfor
  endfor

endfunction


function a()
  [data, Rdata, F, EV, ET, minDS, minL, lambda, fig] = main();

  lambda_len=length(lambda);

  fIdx = (minDS-1) * lambda_len + minL;

  figure(fig++);
  clf;
  hold on;

  title (strcat("Minimal Normalized Error: ", num2str(EV(minL, 2*minDS)), " with lambda: ", \
                num2str(lambda(minL)), " from data-set No. ", num2str(minDS), "."));
  xlabel ("x");
  ylabel ("y");

  plot(data(:,1), data(:,2), "1");
  plot(Rdata(:,1), Rdata(:,2), "+");
  plot(data(:,1), F(:, fIdx));

  print("minerror.png", "-color");
  ## for i=1 : columns(EV)/2
  ##   figure(fig++);
  ##   clf;
  ##   hold on;

  ##   plot(EV(:,2*i-1), EV(:,2*i), num2str(i));
  ##   plot(ET(:,2*i-1), ET(:,2*i), num2str(i));
  ##   xlabel ("Lambda");
  ##   ylabel ("Error");

  ## endfor

  figure(fig++);
  clf;
  hold on;

  leg= cellstr([]);
  for i=1 : columns(EV)/2

    plot(EV(:,2*i-1), EV(:,2*i), num2str(mod(i, 7)));
    leg(i) = strcat("Data-set No. ", num2str(i));

  endfor
  legend(leg);
  xlabel ("Lambda");
  ylabel ("Normalized Error");
  print("errorvslambda.png", "-color");
endfunction


a();
