function Dt = pseudo(lambda, D)
  i=eye(columns(D));
  Dt = inverse(D' * D + (lambda * i)) * D';
endfunction
