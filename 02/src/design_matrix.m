function D = design_matrix(phi, data)
  for i = 1 : rows(data)
    D(i, :) = phi(data(i,1))';
  endfor
endfunction
