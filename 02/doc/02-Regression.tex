\documentclass[a4paper,12pt]{article}
% \usepackage[latin1]{inputenc}
% \usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
% \usepackage[ngerman]{babel}
\usepackage{url}
\usepackage{float}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{graphicx}

% \usepackage{tikzpicture}
\lstset{
  numbers=none,
  frame=single,
  stepnumber=1,
  firstnumber=1,
  numberfirstline=true,
  captionpos=b,
  basicstyle=\footnotesize,
  extendedchars=true,
  literate= {->}{{$\rightarrow$}}1 {-->}{{$\rightarrow$}}2
  {µ}{{$\mu$}}1 {⋅}{{$\cdot$}}1
  {o--}{{\rule[0.5ex]{3mm}{0.1mm}}}2,
}


\title{2 - Ridge Regression and Model Selection}
\author{Klaus Niedermair \& Manuel Schneckenreither}
\date{University Innsbruck\\
  Department for Computer Science\\
  \today}


\begin{document}
\maketitle
\newpage
% \tableofcontents
% \newpage

\section{Algorithmic Choices}

As advised, we decided to use Octave to implement the exercise.

\subsection{Variables/Triggers}
\label{subsec:Variables/Triggers}


The user can set a vector $c$ which represent the polynomial, that will be used
to generate data. This function will show up in most of the plots for easier
comparison of the regression functions, as well as the training and validation
points.

As required the program can be triggered by setting the variables $M$ and $N$,
where $M$ stands for the number of data sets and $N$ for the data points per
data set. Therefore, when $M=3$ and $N=10$ the software will generate $30$
$(x,y)$-pairs and equally split it up giving each data set $m \in M$ exactly
$10$ pairs.

The software was designed from scratch such that the function $\phi(x)$ can be
set to any desired function using Octave's anonymous function syntax, cf.
Listing~\ref{lst:phi}. The presented function in Listing~\ref{lst:phi}
represents the function $w_2 * x^2 + w_1 * x + w_0 * 1$, where the vector
$\vec{w}$ is generated automatically with the length $l$ of the input of
$\phi(x)$.


Furthermore, the tool provides to possibility to change the interval for lambda.
For each data set and each lambda from the given lambda-vector, the tool
generates one graph which will be printed in the figure for the corresponding
data set.

% \begin{equation}
%   \vec{w}=\left(\begin{array}{c} w_0 \\ w_1 \\ \vdots \\ w_n \end{array}\right) \qquad
%   =\qquad w_n*phi(x)^l + \ldots + w_1*x^1 + w_0*x^0 .
% \end{equation}

\begin{lstlisting}[float,label=lst:phi,caption=Anonymous function $\phi(x)$.][b!]
  phi = @(x)[1
  x
  x^2
  ];
\end{lstlisting}


\subsection{Generating Data}
\label{subsec:Generating_data}

Using the vector $c$ the tool generates a polynomial. Then for each data point
in this polynomial a random $\epsilon$ will be added to the $y$-value. To do
this we decided we just pick a number from the normal distribution
$\mathcal{N}(y, \sigma^2)$, where $\sigma$ can be set by the user. Note that the
mean $\mu$ equals $y$ from the corresponding $(x, y)$-value. From this point on
the original $(x, y)$ coordinates will just be used for plotting graphs and is
no more considered for any calculation.


\subsection{Calculation}
\label{subsec:Calculation}

For computing only we center the whole data set in the $y$-direction, by
subtracting the mean of all ranges from every point. Before plotting we add back
the mean, by adding it to the coefficient of $w_0$ and the data points that are
going to be plotted. This is done because, higher polynomial functions tend to
have great numbers as coefficients. This way we can avoid big coefficients and
still get the right result.


For the reason of using K-fold cross validation, the software iterates the data
sets. By doing so it defines the $i^{th}$ data set as the validation set and the
union of all other sets the training set. Then, it generates the design matrix,
which is defined as given in Equation~\ref{eq:dm}, where $n$ is the number of
training data points given. The vector $w$ can then be calculated by solving
$\vec{D}^{-1} * \vec{r}$, where $\vec{r}$ is the vector of $y$-coordinates of
the training data points. Unfortunately, $D$ is most likely not a squared matrix
and therefore not invertible. This yields to the solution of using the
\emph{Moore-Penrose pseudo inverse}. For each $l \in \vec{\lambda}$ the pseudo
inverse is calculated and the solution $w$ can be obtained, where $l$ is used as
penalty term to avoid high-polynomials, see Equation~\ref{eq:D}. This results
from the fact that high-order polynomials (which often over-fit) tend to have
greater coefficients. Therefore, the error function will penalize solutions
having great numbers with the factor of $\lambda * \sum_i{w_i^2}$. This results
in adding $\lambda * I$ to the design matrix, where $I$ is the identity matrix,
see Equation~\ref{eq:D}. Therefore, to finally observe $w$ one only has to
calculate $D^t * \vec{r}$.


\begin{equation}
  \label{eq:dm}
  \vec{D} =
  \left( \begin{array}{ccc}
      {\phi(x_1)}^T \\
      {\phi(x_2)}^T \\
      \vdots\\
      {\phi(x_n)}^T \end{array} \right)
\end{equation}

\begin{equation}
  \label{eq:D}
  D^t = {(D^T * D + \lambda I)}^{-1} * D^T * R
\end{equation}


\subsection{Error}
\label{subsec:Error}

The error function used is given in Equation~\ref{eq:error}, where the function
$g(x_i|w)$ is the polynomial defined by $w$. However, consider that the penalty
term $\lambda * \sum_i{w_i^2}$ was already added by defining the pseudo-inverse
of the design matrix, yielding in the Equation~\ref{eq:error2}.

\begin{equation}
  \label{eq:error}
  E(w|\mathcal{X}) = \frac{1}{2}  \sum_{i=1}^{n}{{(r_i - g(x_i|w))}^2}
\end{equation}


\begin{equation}
  \label{eq:error2}
  E(w|\mathcal{X}) = \frac{1}{2}  \sum_{i=1}^{n}{{(r_i - g(x_i|w))}^2} + \lambda * \sum_j{w_j^2}
\end{equation}


% \subsection{Regularization}
% For computing only we regularize the whole dataset, by subtracting the mean from
% every point. Before we plot we add back the mean. The coefficients, with higher
% polynomial functions tend to be really high, with big data points. This way we
% can avoid big coefficients and still get the right result.

\subsection{K-Fold Cross Validation}
Our whole model selection is based on a K-Cross-Fold-Validation. We start off by
splitting our whole data set into K sets. We iterate over the set of sets, where
k is our validation set and the union of the rest of the set of sets our
training set. For each k we compute the error and compare then with all
other errors.


% \subsection{Regression}
% We take each data point of our data set(the x component), compute the result of
% our presumed Phi function of it and transpose it. The resulting matrix is our
% Designmatrix(D), which, multiplied with our coefficient vector(w) results in R,
% the y component of our data set. But the coefficient vector is what we want as
% result. That’s why we have to compute \begin{math}D^T\end{math}, so we can
% compute
% \begin{math}D^T* R = w \end{math}. For that we need the Moore-Penrose Pseudo
% Inverse. The pseudo inverse is computed like \par\begin{math}(D^T * D + \lambda
%   I)^{-1} * D^T * R \end{math}\par
% Then we can compute our w like \par\begin{math} w = D^T * R \end{math}\par
% wl contains now our coefficients for the polynomial function to approximate the
% data points and looks something like this\par
\section{Numbers, Illustrations and Results}

\subsection{Example 1: A Little Bit Over-Fitted}
\label{subsec:Example_1}

In this example a function $f(x)=0.3x^3 + 0.2x^2 + 2x + 0$ is used to
demonstrate a quite good fit with $\phi=@x[1; x; x^2; x^3; x^4; x^5]$. We
decided to use $M = 3$, $N = 10$ and $\sigma = 50$ for this demonstration. The
following figures show the program calculation and output. Note that each figure
has its own heading and legend with information. The compact blue graph consist
of several single lines. Each line is a computation for one lambda from the set
of lambdas $\{0, 100, \ldots, 25000\}$. The results show that a quite good fit
can be found using ridge regression. First, for all three data sets the errors
decrease quite fast. Then when a minimum is reached they start to increase
again. However, sometimes the minimum is out of sight, like for the data set 3
where the minimum was not reached by the limit of $\lambda = 25000$.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/dataset1.png}
  \caption{\label{fig:od1} Dataset No. 1. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/dataset2.png}
  \caption{\label{fig:od1} Dataset No. 2. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/dataset3.png}
  \caption{\label{fig:od1} Dataset No. 3. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/minerror.png}
  \caption{\label{fig:me} The best fit found by the program. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/errorvslambda.png}
  \caption{\label{fig:od1} The normalized error for the different $\lambda$s. As
  given in the heading of Figure~\ref{fig:me} the best $\lambda = 3900$. }
\end{figure}


\subsection{Example 2: Heavily Over-Fitted}
\label{subsec:Example_2}

This example shows a $9 ^{th}$ order polynomial over-fitting data points
generated from the function $f(x)=0.3x^3 + 0 - 2x + 3434$. The functions try to
fit quite a lot of data points more or less perfectly, this yields to the fact
that at the end, where no data is available no more the functions tend to jump
into one direction. Note that this centering the $\vec{r}$ values was used to
calculate the right result.


\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/overfitted/dataset1.png}
  \caption{\label{fig:od1} Dataset No. 1. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/overfitted/dataset2.png}
  \caption{\label{fig:od1} Dataset No. 2. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/overfitted/dataset3.png}
  \caption{\label{fig:od1} Dataset No. 3. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/overfitted/minerror.png}
  \caption{\label{fig:me2} The best fit found by the program. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/overfitted/errorvslambda.png}
  \caption{\label{fig:od1} The normalized error for the different $\lambda$s. As
  given in the heading of Figure~\ref{fig:me2} the best $\lambda = 100$. }
\end{figure}


\subsection{Example 3: Nice Fit}
\label{subsec:Example_3:_Nice_Fit}

This example fits $3^{rd}$-order polynomial to a $3^{rd}$-order function. The
results are therefore quite good. Again we can see that the linear regression
with $\lambda=0$ is not the best fit. But when using ridge regression with
$lambda=600$ all three data sets decreased the error value.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/nice/dataset1.png}
  \caption{\label{fig:od1} Dataset No. 1. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/nice/dataset2.png}
  \caption{\label{fig:od1} Dataset No. 2. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/nice/dataset3.png}
  \caption{\label{fig:od1} Dataset No. 3. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/nice/minerror.png}
  \caption{\label{fig:me3} The best fit found by the program. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/nice/errorvslambda.png}
  \caption{\label{fig:od1} The normalized error for the different $\lambda$s. As
  given in the heading of Figure~\ref{fig:me3} the best $\lambda = 600$. }
\end{figure}


\subsection{Example 4: Under-Fitting}
\label{subsec:Example_3:_Under-Fitting}

This final example shows what happens if a $2^{nd}$-order polynomial tries to fit
a $3^{rd}$-oder polynomial. Obviously the results can not be satisfying.
However, also in this case different $\lambda$s can improve the result.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/under/dataset1.png}
  \caption{\label{fig:od1} Dataset No. 1. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/under/dataset2.png}
  \caption{\label{fig:od1} Dataset No. 2. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/under/dataset3.png}
  \caption{\label{fig:od1} Dataset No. 3. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/under/minerror.png}
  \caption{\label{fig:me4} The best fit found by the program. }
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/under/errorvslambda.png}
  \caption{\label{fig:od1} The normalized error for the different $\lambda$s. As
  given in the heading of Figure~\ref{fig:me4} the best $\lambda = 5500$. }
\end{figure}


\pagebreak[4]

\section{Discussion, Insights and Observations}

\subsection{Generating samples}
In general, we can say, that our results are greatly depending on the amount of
input data. In case of a lot of input data, the $\lambda$ does not change a lot
for the outcome. However, as it should, in case over-fitting might be a problem
(little data) it plays an important role in the regression. In addition to that
$\sigma$, which defines the normal distribution from which we pick our sample
input data, has the biggest impact on what our result is. E. g.\ if we take a
smaller sigma, compared to our data, the samples are really close to our initial
function from which we take samples, and so the resulting function is quite
exact to the one we took samples off (if we took enough samples). Quite the
opposite occurs, if we take a bigger sigma, compared to the data. Now our
resulting function may vary a lot from the initial one.

This results in the observation that the input data is very important. In case
we would use this method in a real world problem we would spend a lot of effort
to get good and reliable data.

\subsection{Choosing the polynomial function $\phi(x)$}
The outcome also really differs on which polynomial function we choose to start
with. For example if we try to approximate the sinus function with $phi =
@(x)[1; x; x^2; x^3]$, our resulting function does not fit very well the sinus
function. Obviously, the given polynomial is too small to fit the sinus function
well (under-fitting). Now, if we try to approximate the sinus function with a
function like $phi = @(x)[1; x; \ldots; x^{10}]$, the resulting function does
fit really well the samples and the sinus function.

In a real world application we would use the K-fold cross validation over
several polynomials to find out the best polynomial-order for the given data.

\subsection{Choosing our Lambda}
The idea behind $\lambda$ is that to decrease the variance of the data points,
if the number of data points is low and therefore over-fitting might be an
issue. By generating several examples which tended to over-fit, our main
observation was that the distribution of the training points played a very
important role by being able to fit the function. The higher the polynomial the
more important the role of $\lambda$ became. It decreased the importance of the
variance of the data points and therefore, good $\lambda$ values resulted in a
quite smooth function.
\end{document}
