(TeX-add-style-hook "test"
 (lambda ()
    (LaTeX-add-labels
     "subsec:Variables/Triggers"
     "subsec:Generating_data"
     "subsec:Calculation"
     "eq:dm"
     "eq:D"
     "subsec:Error"
     "eq:error"
     "eq:error2")
    (TeX-run-style-hooks
     "graphicx"
     "textcomp"
     "listings"
     "url"
     "babel"
     "ngerman"
     "lmodern"
     ""
     "inputenc"
     "utf8"
     "latex2e"
     "art12"
     "article"
     "12pt"
     "a4paper")))

