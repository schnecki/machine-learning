(TeX-add-style-hook "test2"
 (lambda ()
    (TeX-run-style-hooks
     "graphicx"
     "textcomp"
     "url"
     "babel"
     "ngerman"
     "lmodern"
     ""
     "inputenc"
     "utf8"
     "latex2e"
     "art12"
     "article"
     "12pt"
     "a4paper")))

