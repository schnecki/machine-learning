-- ArgumentOptions.hs ---
--
-- Filename: ArgumentOptions.hs
-- Description:
-- Author: Klaus Niedermair, Manuel Schneckenreither
-- Maintainer:
-- Created: Do Mär 20 13:49:58 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fr Mär 28 12:02:29 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 20
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module Types.ArgumentOptions where

data ArgumentOptions = ArgumentOptions
    { getFilePathTest     :: FilePath
    , getFilePathPred     :: FilePath
    , getClassColumn      :: String
    , getSkipColumns      :: [String]
    , getImpurityFunction :: [Double] -> Double
    , getPrintTree        :: Bool
    , getHelpText         :: Bool
    , getPostPruning      :: [FilePath]
    , getVerbose          :: Bool
    , getMinImpurity      :: Double
    }


--
-- ArgumentOptions.hs ends here
