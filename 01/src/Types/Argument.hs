{-# LANGUAGE CPP #-}
-- Row.hs ---
--
-- Filename: Row.hs
-- Description:
-- Author: Klaus Niedermair, Manuel Schneckenreither
-- Maintainer:
-- Created: Do Mär 20 14:48:36 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fr Mär 28 22:01:14 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 92
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module Types.Argument where

import           Control.DeepSeq

type Rows = [Row]

data Row  = Row
    {
      getColNames :: [Argument]
    , getArguments :: [Argument]
    } deriving (Show, Eq)


data Argument = ArgumentDouble { getItemDouble :: Double }
              | ArgumentString { getItemStr :: String }
              | ArgumentBool { getItemBool :: Bool }
                deriving (Eq)


#ifdef PARALLEL
instance NFData Argument where
    rnf a = a `seq` ()
#endif

instance Show Argument where
    show (ArgumentDouble x) = show x
    show (ArgumentString x) = x
    show (ArgumentBool x)   =show x


instance Ord Argument where
    compare (ArgumentDouble a) (ArgumentDouble b)       = compare a b
    compare (ArgumentString a) (ArgumentString b) = compare a b
    compare (ArgumentBool a) (ArgumentBool b)     = compare a b
    compare a b                                   =
        case a of
          ArgumentDouble {} -> LT
          ArgumentString {} -> case b of
                              ArgumentDouble {} -> GT
                              _ -> LT
          _ -> GT

        -- throw $ FatalException $ "The given class number has different types. " ++
        --       "At least two of {Int, String or Bool}."


type Columns = [Column]

data Column = Column
    { getColumnName :: String
    , getColumnData :: [Argument]
    } deriving (Show)


--
-- Row.hs ends here
