-- DecisionTree.hs ---
--
-- Filename: DecisionTree.hs
-- Description:
-- Author: Klaus Niedermair, Manuel Schneckenreither
-- Maintainer:
-- Created: Do Mär 20 16:36:13 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Do Mär 27 19:21:50 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 73
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Types.DecisionTree where

import Types.Argument

import Data.Tree


-- | The conditions means either <= the given number, equals the string
-- or is always true for CRest. The leaf contains a number of
-- Arguments.
data Condition = CDouble Double | CString String

instance Show Condition where
    show (CDouble x)    = " < " ++ show x
    show (CString x) = " == " ++ x

data NodeLabel = LCondition String Condition | LArgument [Argument]

instance Show NodeLabel where
    show (LCondition s c) = s ++ show c
    show (LArgument ar) = show ar


-- exTree =  Node (LCondition "root" CNone)
--           [ Node (LCondition "age" $ CDouble 22)
--             [ Node (LArgument [ArgumentString "A", ArgumentString "A"]) []

--             , Node (LCondition "age"  $ CDouble 44)
--               [ Node (LArgument [ArgumentString "B", ArgumentString "B"]) []
--               ]
--             ]
--           , Node (LCondition "weight" $ CDouble 103)
--             [ Node (LArgument [ArgumentString "B", ArgumentString "B"]) []
--             , Node (LCondition "grade" $ CDouble 2)
--               [ Node (LArgument [ArgumentString "A", ArgumentString "A"]) []
--               ]
--             ]
--           ]


-- run following to print the example tree

-- putStr $ drawTree $ makeShow $ exTree

makeShow          :: (Show a) => Tree a -> Tree String
makeShow (Node x ch) = Node (show x) $ map makeShow ch


--
-- DecisionTree.hs ends here
