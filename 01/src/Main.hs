{-# LANGUAGE CPP #-}
-- Main.hs ---
--
-- Filename: Main.hs
-- Description:
-- Author: Klaus Niedermair, Manuel Schneckenreither
-- Maintainer:
-- Created: Do Mär 20 13:10:30 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fr Mär 28 20:50:24 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 303
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Main where

import           Types.Argument
import           Types.ArgumentOptions
import           Types.DecisionTree

import           DecisionTree.DecisionTreeBuilder
import           DecisionTree.EvaluateOnTree
import           Exceptions.ExceptionHandler
import           Parsing.Arguments.ArgumentOptionsParser
import           Parsing.DataParser

import qualified Control.Exception                       as E
import           Control.Monad                           (unless, when)
import           Data.List
import           Data.Maybe                              (fromMaybe)
import qualified Data.Set                                as Set
import           Data.Tree
#ifdef DEBUG
import           Debug.Trace                             (trace)
#endif

main :: IO ()
main =
    E.catch
         (do                    -- Read arguments
           maybeArgs <- parseArgOpts :: IO (Maybe ArgumentOptions)
           let args = fromMaybe (E.throw $ FatalException "args where empty") maybeArgs


           columns <- parseFileByColumns (getFilePathTest args)

           -- get the columns
           let newColumns = if null (getClassColumn args)
                            then take (length list - 1) list
                            else removeColumn list (getClassColumn args)
                   where
                     list = foldl removeColumn columns (getSkipColumns args)

               classColumn = if null (getClassColumn args)
                             then last columns
                             else getColumn columns (getClassColumn args)


           -- build decision tree
           when (length columns < 1) $
                E.throw $ FatalException "No input columns found. Did you skip all of them?"

           let tree = buildTree args newColumns classColumn

           -- print tree if desired
           when (getPrintTree args) $ do
                putStrLn $ drawTree $ makeShow tree
                putStr "\n"

           -- read second file, build and print confusion matrix
           unless (null $ getFilePathPred args) $ do
             rows <- parseFileByRows $ getFilePathPred args


             let leaves = evaluateAllRows tree rows
             let classColumnIdx = snd $ fromMaybe (E.throw $ FatalException
                                             "Could not find class columns in prediciton file.")
                                  (find (\x -> fst x == getColumnName classColumn) $
                                        (zip (map show $ getColNames $ head rows) [0..] ))
                 classColumnPred = Column (show $ ( getColNames $ head rows) !! classColumnIdx)
                                     (map (\x -> getArguments x !! classColumnIdx) rows)
                 headers = Set.toList (Set.fromList (getColumnData classColumnPred))
                 lenMat = length headers + 1
                 startMatrix = replicate lenMat $ replicate lenMat 0


                 (failures, resMatrix) = checkResults (0, startMatrix) headers leaves classColumnPred

             -- putStrLn $ unwords  $ map (\x -> show x ++ "\n") $
             --       zip3 (map getArguments rows) leaves (getColumnData classColumnPred)

             -- list of matrix as strings

             let
                 -- result list with column names
                 resultList = (" " : map show headers) : tail
                              (map (\(a, b) -> a : map show (tail b))
                              (zip (" " : map show headers) resMatrix))

                 -- columnlength
                 maxLength = (maximum $ map (maximum . map length) resultList) + 2

                 txt = unwords ( map (\x -> unwords (map (\y -> y ++ replicate (maxLength - length y) ' '
                                      ) x) ++ "\n") resultList)

             putStrLn "Confusion Matrix:\n----------------------------------------\n"
             putStrLn txt

             when (failures > 0)
                  (print $ "Could not class " ++ show failures ++ " attribute vectors.")

             putStrLn "\nFirst column ...real value"
             putStrLn "First row    ...prediction"


         )
         (\e -> print (e :: ProgException))


-- | This function takes as input a list of column and a string and
-- returns the column which is name as the string.
getColumn          :: Columns -> String -> Column
getColumn cols str = fromMaybe
                     (E.throw $ ParseException $ "Could not find column: " ++ str)
                     (find (\x -> str == getColumnName x) cols)


-- | This function takes as input a list of column and a string and
-- returns the list of column without the column named as the string.
removeColumn         :: Columns -> String -> Columns
removeColumn acc str = filter (\x -> getColumnName x /= str) acc
    -- case find (\x -> str == getColumnName (snd x)) (zip [1..] acc) of
    --   Nothing -> E.throw $ ParseException $ "Could not find column: " ++ str
    --   Just x -> take (fst x - 1) acc ++ drop (fst x) acc -- drop the column


--
-- Main.hs ends here
