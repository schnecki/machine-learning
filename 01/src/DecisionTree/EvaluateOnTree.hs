{-# LANGUAGE CPP #-}
-- EvaluateOnTree.hs ---
--
-- Filename: EvaluateOnTree.hs
-- Description:
-- Author: Klaus Niedermair, Manuel Schneckenreither
-- Maintainer:
-- Created: Do Mär 27 14:33:48 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fr Mär 28 21:52:51 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 157
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module DecisionTree.EvaluateOnTree
    ( evaluateFirstColumnElement
    , evaluateAllColumnElements
    , evaluateAllRows
    , evaluateRow
    , checkResults
    ) where


import           Data.Tree

import           Exceptions.ExceptionHandler
import           Types.Argument
import           Types.DecisionTree

#ifdef PARALLEL
import           Control.Parallel.Strategies
#endif
#ifdef DEBUG
import           Debug.Trace                 (trace)
#endif
import           Control.Exception           (throw)
import           Data.List                   (find)
import           Data.Maybe                  (fromMaybe)
import qualified Data.Set                    as Set

evaluateAllColumnElements              :: Tree NodeLabel -> Columns -> [[Argument]]
evaluateAllColumnElements tree columns = evaluateFirstColumnElement tree col0 :
                                         (if null (getColumnData $ head colRest) then []
                                         else evaluateAllColumnElements tree colRest)

    where

      (col0, colRest) = foldl fun ([], []) columns

      fun                        :: (Columns, Columns) -> Column -> (Columns, Columns)
      fun (a0, a1) (Column _ []) = (a0, a1)
      fun (a0, a1) (Column n dt) = (a0 ++ [Column n [head dt]],
                                    a1 ++ [Column n (tail dt)])


evaluateAllRows           :: Tree NodeLabel -> Rows -> [[Argument]]
evaluateAllRows tree rows =
#ifdef PARALLEL
       parMap rdeepseq (evaluateRow tree) rows
#else
       map (evaluateRow tree) rows
#endif

evaluateRow          :: Tree NodeLabel -> Row -> [Argument]
evaluateRow tree (Row ns args) =
    case tree of
      Node (LCondition col cond) (l:r:[]) ->
          case cond of
            CDouble x -> case getArgumentByColName (zip ns args) col of
                           ArgumentDouble y -> if y < x
                                               then evaluateRow l (Row ns args)
                                               else evaluateRow r (Row ns args)
                           _ -> throw $ FatalException $ "Invalid data column types in " ++ col ++ "."

            CString x -> case getArgumentByColName (zip ns args) col of
                           ArgumentString y -> if y == x
                                               then evaluateRow l (Row ns args)
                                               else evaluateRow r (Row ns args)
                           ArgumentBool y -> if show y == x
                                             then evaluateRow l (Row ns args)
                                             else evaluateRow r (Row ns args)
                           _ -> throw $ FatalException $ "Invalid data column types in " ++ col ++ "."

      Node (LArgument leafs) [] -> leafs

      _ -> throw $ FatalException "Leaf with child's or node with more than two child's encountered."


    where
      getArgumentByColName              :: [(Argument,Argument)] -> String -> Argument
      getArgumentByColName list colName = snd $
          fromMaybe
          (throw $ FatalException $ "Could not find column '" ++ colName ++ "' in " ++ show list ++ ".")
          (find (\(x,_) -> show x == colName) list)


evaluateFirstColumnElement           :: Tree NodeLabel -> Columns -> [Argument]
evaluateFirstColumnElement tree columns =
    case tree of
      Node (LCondition col cond) (l:r:[]) ->
          case cond of
            CDouble x -> case head (getColumnData splitColumn) of
                           ArgumentDouble d -> if d < x
                                               then evaluateFirstColumnElement l columns
                                               else evaluateFirstColumnElement r columns
                           _ -> throw $ FatalException $ "Invalid data column types in " ++ col ++ "."
            CString x -> case head (getColumnData splitColumn) of
                           ArgumentString s -> if s == x
                                               then evaluateFirstColumnElement l columns
                                               else evaluateFirstColumnElement r columns
                           ArgumentBool s -> if s == read x
                                             then evaluateFirstColumnElement l columns
                                             else evaluateFirstColumnElement r columns
                           _ -> throw $ FatalException $ "Invalid data column types in " ++ col ++ "."

          where
            splitColumn :: Column
            splitColumn = getColumn columns col


      Node (LArgument leafArgs) [] -> leafArgs
      _ -> throw $ FatalException "Leaf with child's or node with more than two child's encountered."


getColumn           :: Columns -> String -> Column
getColumn cols name = fromMaybe
                      (throw $ FatalException $ "Could not find column: " ++ name ++
                                 " in " ++ show cols ++ "!")
                      (find (\x -> getColumnName x == name) cols)


checkResults :: (Int, [[Integer]]) -> [Argument] -> [[Argument]] -> Column -> (Int, [[Integer]])
checkResults acc _ [] _                              = acc
checkResults (nr, mat) headings (a:as) (Column n dt) =
    case mergeArgumentList a of
      Nothing -> checkResults (nr + 1, mat) headings as (Column n (tail dt))
      Just x -> checkResults (nr, addToMatrix mat x (head dt)) headings
                as (Column n (tail dt))

    where
      addToMatrix                  :: [[Integer]] -> Argument -> Argument -> [[Integer]]
      addToMatrix m pred real = setElem (currentElem + 1) (idxReal, idxPred) m

          where
            currentElem :: Integer
            currentElem = getElem idxReal idxPred m

            idxPred = getHeadingNr pred headings + 1
            idxReal = getHeadingNr real headings + 1

      getHeadingNr        :: Argument -> [Argument] -> Int
      getHeadingNr n strs = fst $ fromMaybe
                            (throw $ FatalException $ "Could not find table heading '" ++ show n ++ "'.")
                            (find (\x -> snd x == n) (zip [0..] strs))

      setElem              :: a -> (Int, Int) -> [[a]] -> [[a]]
      setElem e (m, n) mat =
          take m mat ++ [take n row ++ [e] ++ drop (n+1) row] ++ drop (m+1) mat
          where
            row = mat !! m

      getElem         :: Int -> Int -> [[a]] -> a
      getElem m n mat = (mat !! m) !! n

mergeArgumentList      :: [Argument] -> Maybe Argument
mergeArgumentList args = if length (filter (== maxNr) occurenceList) > 1
                         then Nothing
                         else Just $ fst . head $
                              filter (\x -> snd x == maxNr) (zip set occurenceList)
    where
      maxNr = maximum occurenceList
      occurenceList = map (\x -> length (filter (== x) args)) set
      set = setOfArgs args

setOfArgs :: [Argument] -> [Argument]
setOfArgs list = Set.toList (Set.fromList list)


--
-- EvaluateOnTree.hs ends here
