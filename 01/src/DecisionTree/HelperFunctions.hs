-- HelperFunctions.hs ---
--
-- Filename: HelperFunctions.hs
-- Description:
-- Author: Klaus Niedermair, Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Mär 24 14:14:22 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated:
--           By:
--     Update #: 25
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module DecisionTree.HelperFunctions where

import Control.Monad (liftM)
import Types.ArgumentOptions

-- | This function can be used in a foldl to get the tuple with the
-- minimal value of the second element. The tuples stay in this
-- function the same. If the integer values could be (and are in this
-- module) the index of the list.
findMinimum     :: ArgumentOptions -> (Int, Double) -> (Int, Double) -> (Int, Double)
findMinimum args (ci, cd) (ei, ed)
    | ed < getMinImpurity args = (ci, cd)
    | cd < getMinImpurity args = (ei, ed)
    | cd <= ed                 = (ci, cd)
    | otherwise                = (ei, ed)


findMinimumM            :: ArgumentOptions -> Maybe (Int, Double) -> Maybe (Int, Double) -> Maybe (Int, Double)
findMinimumM _ Nothing b  = b
findMinimumM args a (Just b) = liftM (findMinimum args b) a
findMinimumM _ a Nothing  = a


--
-- HelperFunctions.hs ends here
