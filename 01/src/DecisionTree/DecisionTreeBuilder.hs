{-# LANGUAGE CPP #-}
-- DecisionTreeBuilder.hs ---
--
-- Filename: DecisionTreeBuilder.hs
-- Description:
-- Author: Klaus Niedermair, Manuel Schneckenreither
-- Maintainer:
-- Created: Do Mär 20 17:04:59 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fr Mär 28 22:07:56 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 746
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module DecisionTree.DecisionTreeBuilder
    ( buildTree
    ) where


import           DecisionTree.CalculateImpurity
import           DecisionTree.HelperFunctions
import           Types.Argument
import           Types.ArgumentOptions
import           Types.DecisionTree

import           Control.Exception              (throw)
#ifdef PARALLEL
import           Control.Parallel.Strategies
#endif
import           Data.List                      (sort, sortBy)
import           Data.Maybe                     (fromMaybe)
import           Data.Ord                       (comparing)
import           Data.Tree
import           Exceptions.ExceptionHandler
#ifdef DEBUG
import           Debug.Trace                    (trace)
#endif

-- | This function takes as input the columns that should be
-- considered for building the tree (attributes) and the class column
-- which is the desired output of the decision tree. It returns a TODO
buildTree :: ArgumentOptions -> Columns -> Column -> Tree NodeLabel
buildTree = buildTree'

buildTree'               :: ArgumentOptions -> Columns -> Column -> Tree NodeLabel
buildTree' args cols cla =

    if  not (any (/= Nothing) colsClaTuple) || -- check if there is any split
        imp < getMinImpurity args ||           -- check if we would get below min impurity
        all (== head (getColumnData cla)) (tail $ getColumnData cla) -- check if we are pure
    then Node (LArgument $ getColumnData cla) []
    else Node (LCondition (getColumnName splitColumn)
               (case head $ getColumnData splitColumn of
                  ArgumentDouble {} -> -- use the average of the two
                                       -- ascendant attributes
                      CDouble $ (getItemDouble splitArgument +
                                 getItemDouble splitArgumentSuc) / 2.0
                  -- use the values as given from the split
                  ArgumentString {} -> CString $ getItemStr splitArgument
                  ArgumentBool {} -> CString $ show $ getItemBool splitArgument)
              ) (chldTree leftChld ++ chldTree rightChld )

    where

      -- | This function takes in a [column] and returns it's corresponding tree
      chldTree     :: Columns -> [Tree NodeLabel]
      chldTree col = [buildTree' args (take (length col - 1) (addSplit col)) (last col)]


      -- | This function adds the split argument to the map of the right column.
      addSplit   :: Columns -> Columns
      addSplit c = take colIdx c ++ [c !! colIdx ] ++ drop (colIdx + 1)  c

      -- | (Left Child, right child) of split.
      (leftChld, rightChld) = splitColumns (colIdx, rowIdx) (cols ++ [cla])


      -- | The column which includes the minimal impurity (the one
      -- that defines the split).
      splitColumn :: Column
      splitColumn = (\(Column a b) -> Column a (sort b))  $ cols !! colIdx


      -- | The argument which we split on
      splitArgument :: Argument
      splitArgument = getColumnData splitColumn !! rowIdx


      -- | The successor of the argument we split on.
      splitArgumentSuc :: Argument
      splitArgumentSuc = getColumnData splitColumn !! (rowIdx + 1)


      -- | (Column Index, impurity) of selected split.
      (colIdx, imp) = fromMaybe
                      (throw $ FatalException "No impurity found! Programming error.")
                      (minFun (zip [0..] colsClaTuple))


      minFun :: [(Int, Maybe (Int, Double))] -> Maybe (Int, Double)
      minFun = minFun' Nothing
          where
            minFun' a []           = a
            minFun' Nothing (l:ls) = case snd l of
                                       Nothing -> minFun' Nothing ls
                                       Just (_,e) -> minFun' (return (fst l, e)) ls
            minFun' a  (l:ls)      = case snd l of
                                       Nothing -> minFun' a ls
                                       Just (_,e) -> minFun' (findMinimumM args (return (fst l, e)) a) ls


      -- | [(Row Index, impurity)] of all possible split (one per column)
      (rowIdx, _) = fromMaybe
                    (throw $ FatalException "Want to do impurity over Nothing in colsClaTuple")
                    (colsClaTuple !! colIdx)


      -- | [(Row, Impurity)]
      -- DO THIS IN PARALLEL
      colsClaTuple :: [Maybe (Int, Double)]
      colsClaTuple =
#ifdef PARALLEL
          parMap rdeepseq (\x -> calcImpurity args x cla) cols
#else
          map (\x -> calcImpurity args x cla) cols
#endif

splitColumns                                  :: (Int,Int) -> Columns -> (Columns, Columns)
splitColumns (columIdx, rowIdx) cols =
    case splitElement of
      ArgumentDouble {} -> foldl funInt ([],[]) sortedColumns
      _ -> foldl funEnum ([],[]) cols  -- use the non sorted list!

    where
      funEnum            :: (Columns, Columns) -> Column -> (Columns, Columns)
      funEnum (l, r) col = (l ++ [ Column (getColumnName col) l' ],
                            r ++ [ Column (getColumnName col) r' ])

          where
            (l', r') = foldl (\(l1, r1) x ->
                              if fst x `elem` indexLeftChld
                              then (l1 ++ [snd x], r1)
                              else (l1, r1 ++ [snd x])) ([],[]) (zip [0..] $ getColumnData col)


      -- | The idices which need to be put in the same branch as the
      -- split Element!
      indexLeftChld :: [Int]
      indexLeftChld = map snd $ filter (\a -> fst a == splitElement) (zip (getColumnData colToSort) [0..])


       -- | The enum element we split at.
      splitElement :: Argument
      splitElement = fst $ sortedCol !! rowIdx

      -- | The column with the best impurity (unsorted).
      colToSort :: Column
      colToSort = cols !! columIdx


      funInt                       :: (Columns, Columns)  -> Column -> (Columns, Columns)
      funInt (l, r) (Column n d) = (l ++ [Column n (take (rowIdx + 1) d) ],
                                    r ++ [Column n (drop (rowIdx + 1) d) ] )

      sortedColumns :: Columns
      sortedColumns = foldl putRowInList [] (map snd sortedCol)


      putRowInList        :: [Column] -> Int -> [Column]
      putRowInList [] nr  = map (\(Column n d) -> Column n [d !! nr]) cols
      putRowInList acc nr = map (\(Column n da, Column _ d) ->
                            Column n (da ++ [d!!nr])) (zip acc cols)


      sortedCol :: [(Argument, Int)]
      sortedCol = sortBy (comparing fst) (zip (getColumnData colToSort) [0..])


--
-- DecisionTreeBuilder.hs ends here
