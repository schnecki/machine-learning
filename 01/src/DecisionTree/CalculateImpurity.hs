-- CalculateImpurity.hs ---
--
-- Filename: CalculateImpurity.hs
-- Description:
-- Author: Klaus Niedermaier, Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Mär 24 14:08:52 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fr Mär 28 12:44:25 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 259
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module DecisionTree.CalculateImpurity
    ( calcImpurity
    , missclassificationError
    , entropy
    , giniIndex
    ) where

import           DecisionTree.HelperFunctions
import           Types.Argument
import           Types.ArgumentOptions
import           Types.DecisionTree

import           Control.Exception            (throw)
import           Control.Monad                (mfilter)
import           Data.List                    (foldl', sortBy)
import           Data.Maybe                   (fromMaybe)
import           Data.Ord                     (comparing)
import qualified Data.Set                     as Set
import           Debug.Trace                  (trace)
import           Exceptions.ExceptionHandler

-- | This function takes as input a list of tuples of Arguments. The
-- first element in the tuple is the attribute, the second element the
-- corresponding class (from the same row in the input file) as given
-- in the input file. It returns the best tuple (minimal impurity)
-- containing an index and the impurity.
calcImpurity              :: ArgumentOptions -> Column -> Column ->
                             Maybe (Int, Double)
calcImpurity args col cla =
    if null $ getColumnData col
    then throw $ FatalException "empty column data in calcImpurity"
    else case head $ getColumnData col of
           ArgumentDouble {} -> if null listOfSetImpuritiesInt
                                then Nothing
                                else Just $ getRealIndex $
                                     foldl (findMinimum args) (0, head listOfSetImpuritiesInt)
                                               (zip [1..] $ tail listOfSetImpuritiesInt)

           -- only split between different values it doesn't make
           -- sense to split between (1,1) because there is no
           -- difference. We would not know whether to go down the left
           -- branch or the right branch.

           _ -> if null listOfSetImpurities
                then Nothing
                else Just $ getRealIndex $ foldl (findMinimum args) (0, head listOfSetImpurities)
                         (zip [1..] (tail listOfSetImpurities))

    where

      -- | This variable holds the sorted list of the input List.
      sortedList :: [(Argument, Argument)]
      sortedList = sortBy (comparing fst) (zip (getColumnData col) (getColumnData cla))


      getRealIndex            :: (Int, Double) -> (Int, Double)
      getRealIndex (idx, val) =  (getRealIndex' 0 (map fst sortedList) (setOfArgs !! idx) False,val)


      getRealIndex'               :: Int -> [Argument] -> Argument -> Bool -> Int
      getRealIndex' idx [] _ True = idx - 1
      getRealIndex' idx (l : ls) ref met
          | l == ref  = getRealIndex' (idx + 1) ls ref True
          | met       = idx - 1
          | otherwise = getRealIndex' (idx + 1) ls ref False


      listOfSetImpurities :: [Double]
      listOfSetImpurities = map (fromMaybe (throw $ FatalException "Programming error.")) $
          mfilter (/= Nothing) $ foldl splitBySet [] setOfArgs


      listOfSetImpuritiesInt :: [Double]
      listOfSetImpuritiesInt = map (fromMaybe (throw $ FatalException "Programming error.")) $
                               mfilter (/= Nothing) -- remove all Nothing elements
                                       (take (length list - 1) list) -- cut of the last element

          where
            list = foldl splitBySet [] setOfArgs


      splitBySet         :: [Maybe Double] -> Argument -> [Maybe Double]
      splitBySet acc arg = acc ++ [ impurityOfSplit (getImpurityFunction args)
                                    (filter (\x -> fst x == arg) sortedList)
                                    (filter (\x -> fst x /= arg) sortedList) ]

      -- | All possible values.
      setOfArgs :: [Argument]
      setOfArgs = Set.toList (Set.fromList $ map fst sortedList)


-- | This function takes as input two lists of tuples. Each tuple
-- corresponds to an attribute and the class the origin attribute
-- vector (row in data set) belongs to. The first list is the list
-- before the split (<s) the second list, is the list after the split
-- (>s).
impurityOfSplit     :: ([Double] -> Double) ->
                       [(Argument, Argument)] -> [(Argument, Argument)] -> Maybe Double
impurityOfSplit _ [] _   = Nothing
impurityOfSplit _ _  []  = Nothing
impurityOfSplit iFun h t = Just $
    (fromIntegral (length h) * iFun (phiList h False) +
     fromIntegral (length t) * iFun (phiList t True)) / fromIntegral (length h + length t)


    where

      -- | This function takes as input list of argument tuples and a
      -- Boolean. The list of arguments is used to calculate the phi.
      -- If the second element of the tuples arguments list contains...TODO
      phiList      :: [(Argument, Argument)] -> Bool -> [Double]
      phiList [] _ = throw $ FatalException "Empty list in phiList function. Programming error!"
      phiList l b  = case snd (head l) of
                       ArgumentBool{} -> [p, 1-p]
                           where
                             p = phiOfList (== ArgumentBool b) l
                       -- string or integer as classes (compare over
                       -- the set of classes)
                       _ -> map (\x -> phiOfList (== x) l) $ Set.toList set

                           where
                             -- create a set of unique classes
                             set = foldl (\s x -> Set.insert (snd x) s) Set.empty l


-- | This function is used to calculate the phi value of a list. The
-- input parameters are:
--
-- A function that is used to compare each element with. If it
-- evaluates to true, the element is counted, otherwise not. The
-- second argument is a list of tuple of elements, where the second
-- element is looked at only (the class to compare to)!
--
-- It returns return the percentage of elements satisfying the given
-- function.
phiOfList      :: (Argument -> Bool) -> [(Argument, Argument)] -> Double
phiOfList cmp list = fromIntegral (foldl' fun 0 list) / fromIntegral (length list)
    where
      fun            :: Int -> (Argument, Argument) -> Int
      fun acc (_, e) = if cmp e then acc+1 else acc


-- | This function is a 2 - class impurity measure. It takes as input
-- two doubles and returns the missclassificationError.
missclassificationError   :: [Double] -> Double
missclassificationError p = 1 - maximum p


-- | This function is a 2-class impurity measure. It takes as input
-- two doubles and returns the gini index.
giniIndex   :: [Double] -> Double
giniIndex p = 1 - sum (map sqrt p)


-- | This function is a 2-class impurity measure. It takes as input
-- two doubles and returns the entropy.
entropy   :: [Double] -> Double
entropy p =
    sum (map (\x -> if x <= 0.00
                    then 0.00
                    else x * logBase 2 (1/x)) p)

--
-- CalculateImpurity.hs ends here
