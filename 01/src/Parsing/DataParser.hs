-- DataParser.hs ---
--
-- Filename: DataParser.hs
-- Description:
-- Author: Klaus Niedermair, Manuel Schneckenreither
-- Maintainer:
-- Created: Do Mär 20 14:46:27 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fr Mär 28 18:40:07 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 252
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Parsing.DataParser
    ( parseFileByColumns
    , parseFileByRows
    )
    where

import Types.Argument
import Types.ArgumentOptions

import Text.ParserCombinators.Parsec
import qualified Control.Exception as E
import Exceptions.ExceptionHandler
import qualified Data.Set as Set
import Debug.Trace (trace)
import Control.Exception.Base (throw, SomeException)

parseFileByColumns :: FilePath -> IO Columns
parseFileByColumns file = E.catch
                          (do
                            parseOutput <- parseFromFile columnParser file
                            case  parseOutput of
                              Left x -> E.throw $ ParseException (show x)
                              Right x -> return x
                          )
                          (\e -> throw  $ ParseException $ show (e :: SomeException))


parseFileByRows :: FilePath -> IO Rows
parseFileByRows file = E.catch
                       (do
                         parseOutput <- parseFromFile rowParser file
                         case  parseOutput of
                           Left x -> E.throw $ ParseException (show x)
                           Right x -> return x
                       )
                       (\e -> throw  $ ParseException $ show (e :: SomeException))


seperators :: [Char]
seperators = ";,"


columnParser :: Parser Columns
columnParser = do
  colnames <- arguments
  _ <- newline
  startList <- arguments
  _ <- newline
  rs <- rows (map (: []) startList)
  -- create list of Column
  let res = zipWith Column (map unArgument colnames) rs
  return res

    where
      -- | This function get the String out of the argument type or
      -- throws an error if the type is no ArgumentString.
      unArgument                     :: Argument -> String
      unArgument (ArgumentString x)  = x
      unArgument (ArgumentBool _)    = fail "First column has to name the columns."
      unArgument (ArgumentDouble  _) =
          fail "First column has to name the columns."

rowParser :: Parser Rows
rowParser = do
  colnames <- arguments
  _ <- spaces
  rs <- many1 (row colnames)
  _ <- spaces
  _ <- eof
  return rs


rows :: [[Argument]] -> Parser [[Argument]]
rows list =
  ( do
    _ <- spaces
    _ <- eof
    return list )
    <|> do
      as <- arguments
      let res = map (\(c, b) -> (c++[b])) (zip list as)
      _ <- spaces
      rows res

row          :: [Argument] -> Parser Row
row colNames = do
  args <- arguments
  _ <- spaces
  return $ Row colNames args


arguments :: Parser [Argument]
arguments = do
      a <- (try argumentInt) <|> argumentString
      as <- many1 argument
      return (a:as)


argument :: Parser Argument
argument = do
    _ <- choice (map char seperators) --char ';' <|> char ','
    (try argumentInt) <|> argumentString
            <?> "a string or an integer"


argumentInt :: Parser Argument
argumentInt = do
  f <- char '-' <|> digit
  dig <- many digit
  return $ ArgumentDouble (read (f:dig) :: Double)


argumentString :: Parser Argument
argumentString =
    (do
      _ <- char '"'
      s <- many str
      _ <- char '"'
      return $ createArgument s
      )
    <|> do
      s <- many1 str
      return $ createArgument s

    where
      str = (noneOf $ "\"\n" ++ seperators) <|> oneOf "1234567890"


createArgument :: String -> Argument
createArgument str
    | str == "yes" = ArgumentBool True
    | str == "no"  = ArgumentBool False
    | otherwise    = ArgumentString str


--
-- DataParser.hs ends here
