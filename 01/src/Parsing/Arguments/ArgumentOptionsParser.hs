{-# LANGUAGE CPP #-}
-- ArgumentOptions.hs ---
--
-- Filename: ArgumentOptions.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 21:00:52 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fr Mär 28 12:50:30 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 720
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module Parsing.Arguments.ArgumentOptionsParser
    ( parseArgOpts )
    where


import Types.ArgumentOptions
import DecisionTree.DecisionTreeBuilder
import Exceptions.ExceptionHandler
import DecisionTree.CalculateImpurity

import Data.Foldable (foldlM)
import System.Console.GetOpt
import System.Environment (getArgs, getProgName)
import Control.Exception.Base (throw)
import System.Directory
import Data.Text (split, pack, unpack)

import Debug.Trace (trace)

-- |These are the default options. They are used, if the
-- options do not get set specifically.
defaultOptions :: ArgumentOptions
defaultOptions = ArgumentOptions {
                   getFilePathTest = ""
                 , getFilePathPred = ""
                 , getClassColumn = []
                 , getSkipColumns = []
                 , getImpurityFunction = missclassificationError
                 , getMinImpurity = 0.00
                 , getPrintTree = True
                 , getPostPruning = []
                 , getHelpText = False
                 , getVerbose =  False
                 }

-- |This function defines the options, the function to be called, when
-- the option is set and its help text, in case the -h option gets passed.
--
-- Arguments: The help message to be displayed in case the -h option is set.
options :: [OptDescr (ArgumentOptions -> IO ArgumentOptions)]
options =
  [
   Option ['c'] ["class-column"]
   (ReqArg (\str opts ->
            return $ opts { getClassColumn = str } ) "CLASS_COLUMN" )
   "class column of the input file with its name. If it is not given it is the last non-skipped column"

  , Option ['i'] ["impurity-fun"]
   (ReqArg (\str opts -> do
              let readRes = reads str :: [(Int, String)]
              if null readRes
              then throw $ FatalException "Could not read impurity function. This needs to be a integer value!"
              else do
                let [(nr, _)] = readRes

                return $ opts { getImpurityFunction = case nr of
                                                        2 -> entropy
                                                        1 -> giniIndex
                                                        _ -> missclassificationError
                              } ) "IMPURITY_FUNCTION" )
   "the impurity function to use [0]:\n0\tMissclassification error\n1\tGini index\n2\tEntropy"

  , Option ['h'] ["help"]
   (NoArg (\opts -> return $ opts { getHelpText = True } ))
   "print usage information"


  , Option ['s'] ["skip"]
   (ReqArg (\str opts -> do
              let cols = map unpack $ split (== ',') $ pack str
              return $ opts { getSkipColumns = getSkipColumns opts ++ cols } )
    "SKIP_COLUMNS" )
   "skip the given columns.\n(multiple column names can be separated by a comma: col1,col2,col4)"


  , Option ['p'] ["print-tree-toggle"]
   (NoArg (\opts -> return $ opts { getPrintTree = not (getPrintTree opts) } ))
   "toggle the option to print the decision tree [on]"

  , Option [] ["pre-pruning"]
   (ReqArg (\str opts -> do
             let readRes = reads str :: [(Double, String)]
             let [(nr, _)] = readRes
             if null readRes || nr < 0.00 || nr >= 1.00
             then throw $ FatalException $ "Could not read pre-pruning number.\n " ++
                  "This value has to be in the range  [0,1[ ."
             else return $ opts { getMinImpurity = nr } )
    "DOUBLE" )
   "the value for the minimal impurity. \nAs soon as a split would yield a lower impurity will be deleted from the list of possible splits."

  , Option [] ["post-pruning"]
   (ReqArg (\str opts -> do
              let cols = map unpack $ split (== ',') $ pack str
              return $ opts { getPostPruning = (getPostPruning opts) ++ [str]} )
    "FILE" )
   "path to a file(s) for post pruning. \nCan be separated by commas, or given with several instances of --post-pruning <file>."


  , Option ['v'] ["verbose"]
   (NoArg (\opts -> return $ opts { getVerbose = True } ))
   "verbose output"

  ]


-- |This function parses the Arguments. It gets them and then parses it, and
-- returns a ArgumentOptions object or throws an ioError Exception.
--
-- There is the possiblitiy of optionally giving the filePath. If it is not
-- given, then the default filepath will be taken and a warning will be
-- displayed.
parseArgOpts :: (Monad m) => IO (m ArgumentOptions)
parseArgOpts = do
  argv <- getArgs                                      -- get arguments
  progName <- getProgName                              -- get Program name
  let                                                  -- Create help text
      header = "Usage: " ++ progName ++ " [OPTION...] testFile [predicitonFile]"
      helpMessage = usageInfo header options

      (o, files, err) = getOpt Permute options argv
  -- case errors occured, throw exception, else call functions for each option
  -- selected
  if not $ null err
  then throw $ FatalException $ concat err ++ "\n" ++ helpMessage
  else do
    opt <- foldlM (flip id) defaultOptions o
    if getHelpText opt
    then throw $ ShowTextOnly helpMessage
    else case files of
           [] ->  throw $ ShowTextOnly $ "Error: No input file was given!\n\n" ++
                 helpMessage
           (f:[]) -> return $ return $ opt { getFilePathTest = f }
           (f:f':[]) -> return $ return $ opt { getFilePathTest = f, getFilePathPred = f' }
           _ -> throw $ FatalException "Could not understand argument options. Did you miss a '-'?"

--
-- ArgumentOptions.hs ends here
