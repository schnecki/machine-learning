\documentclass[11pt,a4paper]{scrartcl}
\usepackage{ngerman}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage[babel,german=quotes]{csquotes}
\usepackage{pdfpages}
\usepackage[automark]{scrpage2}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{caption}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{%
  \parbox{\textwidth}{\colorbox{gray}{\parbox{\textwidth}{\hspace{0.15cm}#3}}\vskip-4pt}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}
\lstset{frame=lrb,xleftmargin=\fboxsep,xrightmargin=-\fboxsep}
\newcommand{\np}{Machine Learning}
\newcommand{\exercisesheet}{Exercise Sheet for Decision Trees}
\newcommand{\teamno}{18}
\newcommand{\cm}{Claudio Mair}
\newcommand{\cmmatnr}{1116552}
\newcommand{\ms}{Manuel Schneckenreither}
\newcommand{\msmatnr}{1117198}

\newcommand{\kn}{Klaus Niedermair}
\newcommand{\knmatnr}{1118676}

%opening

\title{\np}
\subtitle{\exercisesheet}
\author{\kn, \knmatnr\\ \ms, \msmatnr}

\pagestyle{scrheadings}
\clearscrheadfoot
\ihead[]{\np}
\ohead[]{\exercisesheet}
\ifoot[]{\kn, \ms}
\ofoot[]{\pagemark}

\begin{document}

\maketitle

\section{Solved Tasks}
\begin{enumerate}
\item Decision Tree
\item Testing Pre-Pruning
% \item P2P
\end{enumerate}

% HOWTO: Include PDF-Files
%\includepdf[pages=1]{01.pdf}

\section{Algorithmic Choices}

\subsection{Handling Data}
\label{subsec:Parsing}

We decided to handle file one as columns instead of rows. So we have
one column per input attribute. This eases the sorting of the
attribute lists. For building trees this makes perfect sense.\\
\\
The when analyzing data, we parse the input file as rows and analyze
each row, one by the other.


\subsection{Impurity Functions}
\label{subsec:Impurity_Functions}

We implemented all three variants of the impurity functions heard in
class. The following formulas were implemented:\\
\\
\begin{math}
  \begin{array}{l l l}

miss(p_i, \dots, p_n) & = 1 - \max (p_i, \ldots, p_n)\\
  \\
gini(p_i, \ldots, p_n) & = 1 - \sum\limits_{i} p_i^2 \\
  \\
entr(p_i, \ldots ,p_n) & = \left\{
    \begin{array}{l l}
      \sum\limits_{i} p_i * \log_2 {\frac{1}{p_i}} &  \quad \text{if $p_i > 0.00$} \\
      0.00 &                                         \quad \text{else}
    \end{array} \right.

\end{array}
\end{math}


\subsection{Pre-Pruning}
\label{subsec:Pre-Pruning}

The program was defined from scratch to use pre-pruning, by setting
the minimal impurity. If there is a split which would yield to a
impurity less than the minimal impurity, the split will be deleted
from the list of all possible splits. The user can enable pre-pruning
with the command line argument $-pre-pruning \hspace{0.25cm} DOUBLE$ by setting the
minimal impurity.

\subsection{Splitting}
\label{subsec:Splitting}


\subsubsection{Splitting on Numbers}
\label{subsubsec:Splitting_on_Numbers}

When splitting on numbers, we decided to change the algorithm from
class a little bit. In class we learned to test ``For all midpoints s
between adjacent values in the sorted sequence''. However, this would
include calculating the split impurity of the midpoint of same values
(e.g.\ the midpoint between adjacent values $1$ and $1$ in the
sorted sequence $(0, 1, 1, 3)$.)\\
\\
Obviously, it does not make sense to distinguish the two attribute
vectors on this attribute, because both have the same value.\\
Therefore, we changed the definition to ``For all midpoints s between
adjacent values in the sorted \textbf{set}''.


\subsubsection{Splitting on Categorical Attributes}
\label{subsubsec:Splitting_on_Categorical_Attributes}

For categorical attributes, there are two possibilites:

\begin{itemize}
\item{Loop over all possible attribute values s of $x_i$, and evaluate
    binary splits by testing for equality $x_i == s$.}
\item{Instead of binary splits, evaluate n-ary splits by the n
    possible attribute values.}
\end{itemize}


For the ease of implementation and evaluation we decided to use binary
splits, instead of n-ary splitting for strings/enums.


\pagebreak


\section{Compiling}
\label{subsec:Compiling}

To compile the program run following command in the right folder. This
presumes an installation of the Haskell ghc compiler and the needed
Haskell libraries. This should work at ZID without any problems.
\\
\\
$ \$ \hspace{0.25cm} make $\\
\\
\\
To enable \textbf{parallel support} you need to have the module
$parallel$ installed. You can do this by running $cabal
\hspace{0.25cm} install \hspace{0.25cm} parallel$.
Afterwards compile with:\\
\\
\\
$ \$ \hspace{0.25cm} make \hspace{0.25cm}  DEF=PAR $ \\
\\
\\
However, on the machine we tested the parallel version (Linux (Fedora
20, 64-bit), 8 cores (8MB Cache), 30GB RAM) we could not see a major
change in the program execution time. This actually seems to happen
for the reason that Haskell uses automatic parallelism. So the
`sequential' version is actually already using multiple cores, if
available.

\subsection{Alternative using Cabal}
\label{subsubsec:alternative}

If you have cabal installed (which is installed at ZID, but however
does not work appropriately there) run the following commands:
\\
\\
$ \$ \hspace{0.25cm} cabal \hspace{0.25cm} configure $\\
$ \$ \hspace{0.25cm} cabal \hspace{0.25cm} build $

\pagebreak
\section{Usage}
\label{sec:Usage}

The help text output is quite useful when trying to find out the usage
of the program.

\begin{lstlisting}[caption=Help Text]

./dt [OPTION...] testFile [predicitonFile]
  -c CLASS_COLUMN       --class-column=CLASS_COLUMN
  -i IMPURITY_FUNCTION  --impurity-fun=IMPURITY_FUNCTION


  -h                    --help
  -s SKIP_COLUMNS       --skip=SKIP_COLUMNS
  -p                    --print-tree-toggle
                        --pre-pruning=DOUBLE
  -v                    --verbose

\end{lstlisting}

The simplest usage of the program is to let it draw a tree giving a
file as the command parameter. One can set the class column by giving
the name of the column. In the same manner one can skip several column
of the input files by adding multiple files, separated by a comma.
Example command:\\
\\

$ \$\hspace{0.25cm}./dt\hspace{0.25cm}<file1>\hspace{0.25cm}[file2]\hspace{0.25cm}-c\hspace{0.25cm}age\hspace{0.25cm}-s\hspace{0.25cm}date,balance,default $\\

\pagebreak
\section{Example Output}
\label{subsec:example_output}


\subsection{Decision Tree}
\label{subsubsec:Decision_Tree}

One can toggle the output of the decision tree with the $-p$ option.
Below is a small illustration of the program's decision tree output.
The input file used is $test2.csv$ which should be available in the
folder $doc/examples/$.


\begin{lstlisting}[caption=Decision Tree Output]
  job == services
  |
  +- age < 41.0
  |  |
  |  +- age < 36.0
  |  |  |
  |  |  +- [True]
  |  |  |
  |  |  `- [False]
  |  |
  |  `- [True]
  |
  `- age < 28.5
     |
     +- [True]
     |
     `- age < 29.5
        |
        +- [True]
        |
        `- [False,False,False,False]

\end{lstlisting}

The output should be quite intuitive. For each node there is a binary
split, on which when answered $True$ (or $yes$) the first branch is to
be picked, otherwise the second one. Each leaf holds the classes from
the training data. In this case the classes are Boolean values.

\pagebreak

\subsection{Confusion Matrix}
\label{subsubsec:Confusion_Matrix}


Below is a sample output of the decision tree given above using some
random sample data.

\begin{lstlisting}[caption=Sample Output for the Confusion Matrix]

        False   True
 False   34      3
 True    2       1


First column ...real value
First row    ...prediction


\end{lstlisting}


If the tree is not completely impure (this can happen when same
training attribute vectors with different classes exits) then the
program will let you know, if and how may attribute vectors could not
be classified. This means that at the leaf of the decision tree, there
was an equal class ration. Therefore it was impossible to specify a
class for the given attribute vector.

\pagebreak

\section{Results, Insights and Observations}
\label{sec:Results,_Insights_and_Observations}


Basic steps of our application (abstracted):

\begin{itemize}{}

\item{Read and parse file 1 and 2 (learn and test file).}
\item{Take file 1 and apply algorithm on it:}
\begin{itemize}{}
\item{Unless the minimum impurity value is reached or no further
    splits can be executed, split on the least impurity, of all
    possible splits.}
\item{Take the two children and start splitting again.}

\end{itemize}
\item{Take file 2 and apply for every line in it:}
\begin{itemize}{}
\item{Follow the tree, with the values we get out of a line.}
\item{Until we end up in a leaf.}
\item{Compare the value in the leave, with the class value given in file 2.}
\item{Fill in correct fields of confusion matrix.}
\end{itemize}


\end{itemize}

We tested our decision tree application for 6 sets of data and the
results we get, heavily depended on the input, type of the data and
the columns we select as the classification column and the attribute
columns. For example, the result of a bank data did not give a
definite classification tree. The confusion matrix resulted in a wide
spread matrix, with a small peak in the diagonal. Then we applied our
decision tree on a set of chess matches (board placements on the last
turn) which resulted in a definite confusion matrix. The impurity
measure function (entropy, miss-classification, giniIndex) had just a
small impact on the obtained decision tree. The tree's itself
differed, but not so much that we could say, this one is better/worse.


\subsection{Pruning}
\label{subsec:Pruning}

For a pruning test we used following command:\\

$ \$ \hspace{0.25cm} ./dist/build/dt/dt \hspace{0.25cm} ./doc/examples/bank/bank.csv \hspace{0.25cm} ./doc/examples/bank/bank-full.csv \hspace{0.25cm} -p \hspace{0.25cm} --pre-pruning \hspace{0.25cm} x $\\
\\
Where x stands for a double value. The following table show the
results we observed by pre-pruning on different values.\\
\\
% BEGIN RECEIVE ORGTBL pruning
\begin{tabular}{rrrrr}
x & False Positives & False Negatives & Not classified & Sum of Misscl. Attributes \\
\hline
0.2 & 0 & 5289 & 0 & 5289 \\
0.15 & 0 & 5289 & 0 & 5289 \\
0.1 & 244 & 3385 & 358 & 3629 \\
0.075 & 804 & 3528 & 1933 & 4332 \\
0.05 & 1378 & 2892 & 3428 & 4270 \\
0.00 & 3357 & 3023 & 0 & 6380 \\
\end{tabular}
% END RECEIVE ORGTBL pruning
\\
\\
Where $False \hspace{0.25cm} Positives$ stands for attribute vectors
classified as $True$ but actually being of class $False$ and vice
versa for $False \hspace{0.25cm}
Negatives$.\\
\\
In this example the best pre-pruning factor is somewhere near $0.1$
for the minimal impurity measure. However, the $false \hspace{0.25cm}
negatives$ are
lowest with a minimal impurity of 0.05 with the tested factors.\\
\\
This show actually how hard it is to determine the best prune
impurity. Especially, if the data applied to the tree changes over
time (and with it the best impurity-pre-pruning factor) the results
will get worse over time. This yields to the result that a
recalculation of the decision tree (and getting data for this task)
has to be done periodically.

\end{document}

#+ORGTBL: SEND parallelism orgtbl-to-latex :splice nil :skip 0
| Test | Sequential | Parallel  |
|------+------------+-----------|
|    1 |            | 1m33.983s |
|    2 |            | 1m33.689s |
|    3 |            | 1m31.032s |
|    4 |            | 1m32.097s |


#+ORGTBL: SEND pruning orgtbl-to-latex :splice nil :skip 0
|     x | False Positives | False Negatives | Not classified | Sum of Misscl. Attributes |
|-------+-----------------+-----------------+----------------+---------------------------|
|   0.2 |               0 |            5289 |              0 |                      5289 |
|  0.15 |               0 |            5289 |              0 |                      5289 |
|   0.1 |             244 |            3385 |            358 |                      3629 |
| 0.075 |             804 |            3528 |           1933 |                      4332 |
|  0.05 |            1378 |            2892 |           3428 |                      4270 |
|  0.00 |            3357 |            3023 |              0 |                      6380 |
#+TBLFM: $5=$2+$3
